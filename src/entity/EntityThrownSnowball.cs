﻿using rtchristmas.src.util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Vintagestory.API.Common;
using Vintagestory.API.Common.Entities;
using Vintagestory.API.MathTools;
using Vintagestory.API.Server;
using Vintagestory.GameContent;

namespace rtchristmas.src.entity
{
    public class EntityThrownSnowball : EntityThrownStone
    {
        public float SnowballDamage = 3;

        public override void Initialize(EntityProperties properties, ICoreAPI api, long InChunkIndex3d)
        {
            base.Initialize(properties, api, InChunkIndex3d);

            var attrib = properties?.Attributes["ThrownDamage"]?.ToString();
            if (!string.IsNullOrEmpty(attrib) && float.TryParse(attrib, out var newDamage))
                SnowballDamage = newDamage;
        }

        public override void OnGameTick(float dt)
        {
            if (ShouldDespawn)
                return;

            this.Damage = SnowballDamage;

            base.OnGameTick(dt);

            if (!Collided || !(World is IServerWorldAccessor))
                return;

            double speed = SidedPos.Motion.Length();
            if (speed < 0.01)
            {
                World.SpawnCubeParticles(SidedPos.XYZ.OffsetCopy(0, 0.2, 0), ProjectileStack, 0.2f, 20);
                this.Die();
            }
        }
    }
}
