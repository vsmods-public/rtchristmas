﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vintagestory.API.Common;
using Vintagestory.GameContent;

namespace rtchristmas.src
{
    public class ItemSnowball : ItemStone
    {
        public override void GetHeldItemInfo(ItemSlot inSlot, StringBuilder dsc, IWorldAccessor world, bool withDebugInfo)
        {
            base.GetHeldItemInfo(inSlot, dsc, world, withDebugInfo);

            var newDamage = this.Attributes["ThrownDamage"]?.ToString();
            if (newDamage != null && float.TryParse(newDamage, out var floatDamage))
                dsc.Replace("1", floatDamage.ToString());
        }
    }
}
