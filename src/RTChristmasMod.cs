﻿using rtchristmas.src.entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vintagestory.API.Common;

namespace rtchristmas.src
{
    public class RTChristmasMod : ModSystem
    {
        public override bool ShouldLoad(EnumAppSide forSide)
        {
            return true;
        }

        public override void Start(ICoreAPI api)
        {
            base.Start(api);

            api.RegisterEntity("entitythrownsnowball", typeof(EntityThrownSnowball));

            api.RegisterItemClass("ItemSnowball", typeof(ItemSnowball));
        }
    }
}
